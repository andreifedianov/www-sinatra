class ImageFile
  include DataMapper::Resource
  property :id, Serial
  property :original_name, Text, :required => true
  property :new_name, Text, :required => true
  property :created_at, DateTime
  property :updated_at, DateTime
  property :deleted, Integer, :default => 0

  def self.default_repository_name
    :default
  end
end

DataMapper.finalize
DataMapper.auto_upgrade!

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end
