class Visitor
  include DataMapper::Resource
  property :id, Serial
  property :ip_address, Text, :required => true
  property :visit, Integer, :default => 0
  property :agent, Text
  property :page, Text
  property :created_at, DateTime
  property :updated_at, DateTime
  def self.default_repository_name
    :default
  end
end

DataMapper.finalize
DataMapper.auto_upgrade!

helpers do
  include Rack::Utils
  alias_method :hh, :escape_html
end
