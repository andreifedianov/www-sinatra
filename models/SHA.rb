class SHA
  include DataMapper::Resource
  property :id, Serial
  property :original, Text, :required => true
  property :content, Text, :required => true
  property :created_at, DateTime
  property :updated_at, DateTime
  property :found, Integer, :default => 1

  def self.default_repository_name
    :default
  end
end

DataMapper.finalize
DataMapper.auto_upgrade!

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end
