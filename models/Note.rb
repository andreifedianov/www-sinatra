require 'data_mapper'
class Note
  include DataMapper::Resource
  property :id, Serial
  property :content, Text, :required => true
  property :complete, Boolean, :required => true, :default => 0
  property :created_at, DateTime
  property :updated_at, DateTime
  property :deleted, Boolean, :default => false

  def self.default_repository_name
    :default
  end
end


DataMapper.finalize
DataMapper.auto_upgrade!

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end
