configure :development do
  DataMapper.setup :default, {
    :adapter	=> 'mysql',
    :host		=> 'localhost',
    :username	=> 'root',
    :password	=> '',
    :database 	=> 'development'
  }
  # DataMapper.auto_migrate!
  DataMapper.auto_upgrade!
  DataMapper::Logger.new(STDOUT, :debug)
end


configure :production do
  DataMapper.setup :default, {
    :adapter	=> 'mysql',
    :host		=> 'localhost',
    :username	=> 'root',
    :password	=> '',
    :database 	=> 'production'
  }

  # DataMapper.auto_migrate!
  DataMapper.auto_upgrade!
end

require './models/Note.rb'
require './models/Visitor.rb'
require './models/SHA.rb'
