allowed = ["image/jpeg", "image/pjpeg", "image/png", "image/x-png", "image/gif"]

get '/i/' do
  redirect "/i"
end

get '/upload/' do
  redirect "/upload"
end

get '/i' do
  @title = "Like imgur, but not really"
  erb :upload
end

get '/upload' do
  @title = "Like imgur"
  erb :upload
end

post '/upload' do
  # todo, check for file size before upload, cuz we got some huge files that can be uploaded.
  if params[:file].nil?
    @notice = "No file selected :("
    erb :upload
  end

  # logger.info params[:file].size
  if allowed.include? params[:file][:type]
    image = MiniMagick::Image.read(params[:file][:tempfile])
    ext = params[:file][:type].split("/")[1]
    image.format ext
    filename = generate_filename
    image.write "public/i/#{filename}.#{ext}"
    image.sample "50x50"
    image.gravity "center"
    image.write "public/i/thumbs/#{filename}.gif"
    redirect "/i/#{filename}.#{ext}"
  else
    @notice = "Wrong file format"
    erb :upload
  end
end

# post '/upload' do
#   unless params[:file] &&
#       (tmpfile = params[:file][:tempfile]) &&
#       (name = params[:file][:filename])
#     @error = "No file selected"
#   end
#   STDERR.puts "Uploading file, original name #{name.inspect}"
#   f = File.open("public/#{name}", "w")
#   while blk = tmpfile.read(65536)
#     # here you would write it to its final location
#     # STDERR.puts blk.inspect
#     f.write(blk)
#   end
#   "Upload complete http://mc.mxdm.net/#{name}"
# end

def generate_filename(length = 10)
  o = [('a'..'z'),('A'..'Z'),('0'..'9')].map{|i| i.to_a}.flatten
  string = (0...length).map{ o[rand(o.length)] }.join
  return string
end
