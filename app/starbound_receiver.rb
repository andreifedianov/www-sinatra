require "bunny"
c = Bunny.new
c.start

ch = c.create_channel
x = ch.fanout("starbound_logs")
q = ch.queue("", :exclusive => true)

q.bind(x)

begin
	q.subscribe(:block => true) do |delivery_info, properties, body|
		puts "#{body}"
	end
rescue Interrupt => _
	ch.close
	c.close
end