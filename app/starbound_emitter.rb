require "bunny"
conn = Bunny.new
conn.start

channel = conn.create_channel
exchange = channel.fanout("starbound_logs")

msg = `netstat -n | grep -c 21025`

exchange.publish(msg)

conn.close