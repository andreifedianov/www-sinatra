<?php

/**
 * This is a test script to send a message to an APNS device, i.e. an Apple iOS device using the APNS. 
 * 
 * To use this script you need to set the following 4 peieces of information
 *  - message
 *  - device token
 *  - certificate file
 *  - pass phrase
 *
 * Then run it using php: 
 * <code> php simplepush.php </code> 
 *
 */

// Message to send
$message = $_POST['msg']; //'the test message';

// Put your device token here (without spaces):
if(sizeof($_POST['tkn']) > 1){
	$deviceToken = $_POST['tkn'];
} else {
	$deviceToken = '58960814683af94e81567d40bbaf2ebdf2415a751f1cad78284657a9cee4f324';
}


//
//	Certificate Key Details
$certfile = 'apns-dev-cert.pem';
// Put your private key's passphrase here:
$passphrase = '0n3tw0thr33';





////////////////////////////////////////////////////////////////////////////////



$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', $certfile);
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'badge' => 0,
	'sound' => 'default'
	);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
?>