require 'sinatra'
require 'data_mapper'
require 'builder'
require 'time'
require 'base64'
require 'find'
require 'mini_magick'
require 'mysql2'
require 'logger'
require './app/upload_controller.rb'
require './app/starbound.rb'
#require 'rack-flash'
#require 'sinatra/redirect_with_flash'
require './models/Models.rb'
require 'digest/sha1'
require 'digest/sha2'
require 'digest/md5'
require 'logger'
enable :sessions
#use Rack::Flash, :sweep => true

#main.rb

SITE_TITLE = "mc"
SITE_DESCRIPTION = ""


get '/' do
  putVisitor 'index'
  erb :index
end

get '/notes/' do
  redirect '/notes'
end

get '/notes' do
  @notes = Note.all :order => :id.desc
  @title = 'All Notes'
  erb :home
end

post '/notes' do
  n = Note.new
  n.content = params[:content]
  n.created_at = Time.now
  n.updated_at = Time.now
  n.save
  redirect '/notes'
end

get '/who' do
  putVisitor 'who'
  @visitors = Visitor.all :order => :updated_at.desc
  @title = "Recent Visitors"
  erb :visitors
end
get '/agent' do
  @title = "Secret Agent Man"
  erb :agent
end

get '/test' do
  redirect '/'
end

get '/form' do
  erb :form
end

post '/form' do
  "You said '#{params[:message]}'"
end

get '/secret' do
  @title = "SHA2 Encoder"
  erb :secret
end

post '/secret' do
  @title = "SHA2 Encoded"
  @data = Digest::SHA2.hexdigest(params[:secret])
  sha = SHA.last(:content => @data)
  if(sha.nil?)
    sha = SHA.new
    sha.original = params[:secret]
    sha.content = @data
    sha.created_at = Time.now
    sha.updated_at = Time.now
    sha.save
  else
    sha.update(:found => sha.found + 1, :updated_at => Time.now)
  end
  erb :secret
end


get '/notes/rss.xml' do
  @notes = Note.all :order => :id.desc
  builder :rss
end

get '/ohyeah' do
  putVisitor 'ohyeah'
  @arr = get_files '/var/www_sin/rawr/public/ohyeah'
  SITE_DESCRIPTION = SITE_DESCRIPTION + "."
  @title = "Yo"
  erb :ohyeah
end

get '/notes/:id' do
  @note = Note.get params[:id]
  @title = "Edit note ##{params[:id]}"
  erb :edit
end

put '/notes/:id' do
  n = Note.get params[:id]
  n.content = params[:content]
  n.complete = params[:complete] ? 1 : 0
  n.updated_at = Time.now
  n.save
  redirect '/notes'
end

get '/notes/:id/delete' do
  @note = Note.get params[:id]
  @title = "Confirm deletion of note ##{params[:id]}"
  erb :delete
end

delete '/notes/:id' do
  n = Note.get params[:id]
  n.deleted = true
  n.save
  redirect '/notes'
end

get '/notes/:id/complete' do
  n = Note.get params[:id]
  n.complete = n.complete ? 0 : 1
  n.updated_at = Time.now
  n.save
  redirect '/notes'
end

#error 400..510 do
#@title = "Don't panic!"
#erb :"error/notfound"
#end

error 403 do
  'Access forbidden'
end

not_found do
  @title = "Don't panic!"
  erb :"error/notfound"
end

def get_files path
  dir_array = Array.new
  Find.find(path) do |f|
    if File.basename(f)[0] == "."
      Find.prune
    elsif !File.directory?(f)
      dir_array << File.basename(f)
      if(!File.exists? "/var/www_sin/rawr/public/thumbs/" + File.basename(f))
        image = MiniMagick::Image.open(f)
        image.resize "100x100"
        image.write "/var/www_sin/rawr/public/thumbs/" + File.basename(f)
      end
    end
  end
  return dir_array
end

def putVisitor page
  ipaddr = request.env['REMOTE_ADDR']
  agent = request.env['HTTP_USER_AGENT']
  v = Visitor.last(:ip_address => ipaddr)
  if v.nil?
    v = Visitor.new
    v.ip_address = ipaddr
    v.visit = 0
    v.agent = agent
    v.page = page
    v.created_at = Time.now
    v.updated_at = Time.now
    v.save
  else
    v.update(:visit => v.visit + 1, :updated_at => Time.now, :page => page, :agent => agent)
  end
end